
'''
ModPlug Tracker  IT
|C-530v54...|D#530v54...|C-402v54...
|........122|...........|...........
|........444|...........|...........
|.....v08...|.....v08...|===02......
|........ZGE|...........|...........
|...........|...........|...........
'''

#Register Hotkey Constants

NOTE_OFF = 255
NOTE_CUT = 254
NOTE_FADE = 253
NOTE_NONE = -1
notenames = ['C-','C#','D-','D#','E-','F-','F#','G-','G#','A-','A#','B-']


def mpt_get_note_number(data):
    nn = NOTE_NONE
    if data in ['',"..."]:
        nn = NOTE_NONE
    elif data == "===":
        nn = NOTE_OFF
    elif data == "^^^":
        nn = NOTE_CUT
    elif data == "~~~":
        nn = NOTE_FADE
    else:
        n = notenames.index(data[0:2])
        o = int(data[2:3],16)
        nn = o * 12 + n
    return nn

def mpt_get_note_name(nn):
    if nn == NOTE_NONE: 
        return "..."
    elif nn == NOTE_OFF:
        return "==="
    elif nn == NOTE_CUT:
        return "^^^"
    elif nn == NOTE_FADE:
        return "~~~"
    else:
        n = nn % 12
        o = nn // 12
        return "%s%X" % (notenames[n],o)

def mpt_get_note_info(nn):
    return { 'n': nn % 12, 'o': nn // 12, 'off': (nn == NOTE_OFF), 'cut': (nn == NOTE_CUT), 'fade': (nn == NOTE_FADE), 'none': (nn == NOTE_NONE) }

ORDER_DIVIDER = 254
ORDER_END = 255

def mpt_split_song(data):
    '''
    Returns
    { speed: int
      tempo: int
      rpb: int
      rpm: int
      title: str
      channels: 
      [{ vol: int
        pan: int
        title: str }]
      orderlist: [ int ]
      patterns 
      [{ rpb: int
        rpm: int
        title: str
        rows = [ see mpt_split_pattern ] }]
    }
    '''
    lines = data.splitlines()
    if lines[0] != "Modplug Tracker Song Data":
        return None
    h = [int(x) for x in lines[1].split(",")]
    song = {}
    chanCount = h[0]
    ordCount = h[1]
    patCount = h[2]
    for k,v in zip(['speed','tempo','rpb','rpm'],[3,4,5,6]):
      song[k] = h[v]
    song['title'] = lines[2]
    chanvols = [int(x) for x in lines[3].split(',')]
    chanpans = [int(x) for x in lines[4].split(',')]
    chantitles = lines[5:(5+chanCount)]
    song['channels'] = [{'vol': v, 'pan': p, 'title': t} for v,p,t in zip(chanvols,chanpans,chantitles)]
    lnum = 5+chanCount
    song['orderlist'] = [int(x) for x in line[lnum].split(',')]
    lnum += 1
    pats = []
    for i in xrange(patCount):
      pat = {}
      h = [int(x) for x in lines[lnum]]
      rowCount = h[0]
      lnum += 1
      pat['rpb'] = h[1]
      pat['rpm'] = h[2]
      pat['title'] = lines[lnum]
      lnum += 1
      pat['rows'] = '\r\n'.join(lines[lnum:(lnum+rowCount)])
      lnum += rowCount
      pats.append(pat)
    song['patterns'] = pats
    return song

def mpt_split_pattern(data):
    '''
    Returns
    [ #row
        [ #col
            { note: int,
              inst: int,
              volcmd: str,
              volval: int,
              effcmd: str,
              effval: int }
        ]
    ]
    '''
    lines = data.splitlines()
    if lines[0] != "ModPlug Tracker  IT":
        return None
    rows = []
    for row in lines[1:]:
        columns = row[1:].split("|")
        row = []
        for col in columns:
            colm = {}
            colm['note'] = mpt_get_note_number(col[0:3])
            colm['inst'] = int(col[3:5]) if col[3:5] not in ['','..'] else -1
            colm['volcmd'] = col[5:6]
            colm['volval'] = int(col[6:8]) if col[6:8] not in ['','..'] else -1
            colm['effcmd'] = col[8:9]
            colm['effval'] = int(col[9:11],16) if col[9:11] not in ['','..'] else -1
            row.append(colm)
        rows.append(row)
    return rows

def mpt_join_pattern(rows):
    lines = []
    lines.append("ModPlug Tracker  IT")
    for rowm in rows:
        row = ""
        for colm in rowm:
            row += "|"
            row += mpt_get_note_name(colm['note'])
            row += "%02d" % colm['inst'] if colm['inst'] > 0 else ".."
            row += colm['volcmd'] if colm['volcmd'] != '' else '.'
            row += "%02d" % colm['volval'] if colm['volval'] > -1 else ".."
            row += colm['effcmd'] if colm['effcmd'] != '' else '.'
            row += "%02X" % colm['effval'] if colm['effval'] > -1 else '..'
        lines.append(row)
    return '\r\n'.join(lines)


ENV_HEADER_1 = ['sustain_start','sustain_end','loop_start','loop_end']
ENV_HEADER_2 = ['has_sustain','has_loop','has_carry']

def mpt_split_env(data):
    '''
    Returns 
    { sustain_start: int,
      sustain_end: int,
      loop_start: int,
      loop_end: int,
      has_sustain: bool,
      has_loop: bool,
      has_carry: bool 
      points: [ { x: int, y: int }] }
    '''
    lines = data.splitlines()
    if lines[0] != "Modplug Tracker Envelope":
        return None
    headervals = lines[1].split(",")
    nodes = int(headervals[0])
    header = {}

    for x in xrange(1,5):
        header[ENV_HEADER_1[x-1]] = int(headervals[x])
    for x in xrange(5,8):
        header[ENV_HEADER_2[x-5]] = (int(headervals[x]) == 1)
    off = 2
    points = []
    for n in xrange(nodes):
        x,y = [int(x) for x in lines[off+n].split(",")]
        points.append({ "x": x, "y": y })
    header['points'] = points
    return header

def mpt_join_env(header):
    lines = []
    points = header['points']
    lines.append("Modplug Tracker Envelope")
    lines.append( 
        ','.join(
        ["%d" % len(points)] + 
        ["%d" % header[key] for key in ENV_HEADER_1] +
        [("1" if header[key] else "0") for key in ENV_HEADER_2]
        )
        )
    for n in points:
        lines.append("%d,%d" % (n['x'],n['y']))
    lines.append("255")
    return '\r\n'.join(lines)

def mpt_new_env():
    return {
        'sustain_start': 0,
        'sustain_end': 0,
        'loop_start': 0,
        'loop_end': 0,
        'has_sustain': False,
        'has_loop': False,
        'has_carry': False,
        'points': []
    }
def mpt_new_point(x,y):
    return {'x':x,'y':y}
