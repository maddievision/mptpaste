from pastelib import *

def NotesToPitchEnv(data):
    rows = mpt_split_pattern(data)
    if not rows:
        MsgBox("Need Pattern Data! Copy pattern pls")
        return
    col1 = [x[0] for x in rows]
    basenote = -1
    x = 0
    env = mpt_new_env()
    startpoint = 0
    endpoint = 0
    ly = -1
    inturd = -1
    steplen = -1
    for row in col1:
        if row['effcmd'] == 'S':
            if row['effval'] == 0xB0:
                startpoint = len(env['points'])
            elif row['effval'] == 0xBF:
                env['has_carry'] = True
            elif row['effval'] == 0xB1:
                endpoint = len(env['points'])+2
                env['has_loop'] = True
            else:
                endpoint = len(env['points'])+2
                env['has_sustain'] = True

        if basenote == -1:
            basenote = row['note']
        if row['inst'] > 0:
            steplen = row['inst']
        if row['note'] not in [NOTE_OFF,NOTE_NONE,NOTE_FADE,NOTE_CUT]:
            print row['note']
            inturd = 32 + ((row['note'] - basenote) * 2)
        if inturd < 0: inturd = 0
        if inturd > 64: inturd = 64

        if ly > -1:
            env['points'].append(mpt_new_point(x,ly))
        if ly != inturd:
            env['points'].append(mpt_new_point(x,inturd))

        ly = inturd
        x += steplen

    env['points'].append(mpt_new_point(x,ly))
    if env['has_loop']:
        env['loop_start'] = startpoint
        env['loop_end'] = endpoint
    elif env['has_sustain']:
        env['sustain_start'] = startpoint
        env['sustain_end'] = endpoint
    return mpt_join_env(env)

def SuperCpy2(data): pass

def SuperCpy(data):
    rows = mpt_split_pattern(data)
    rowcount = len(rows)
    colcount = len(rows[0])
    colprops = []
    for i,c in enumerate(map(list, zip(*rows))):
        #find first note
        for j,r in enumerate(c):
            if r['note'] not in [NOTE_OFF,NOTE_NONE,NOTE_FADE,NOTE_CUT]:
                col = {}
                col['offset'] = j
                col['note'] = r['note']
                col['volval'] = r['volval'] if r['volcmd'] in ['v','o','p'] else -1
                col['volcmd'] = r['volcmd'] if r['volcmd'] in ['v','o','p'] else ''
                col['effval'] = r['effval'] if r['effcmd'] in ['O','S'] else -1
                col['effcmd'] = r['effcmd'] if r['effcmd'] in ['O','S'] else ''
                col['inst'] = r['inst']
                colprops.append(col)
                break

    col1 = [x[0] for x in rows]
    cp = colprops[0]
    for j,r in enumerate(col1):
        for i,c in enumerate(colprops):
            if i == 0: continue
            offset = c['offset'] - cp['offset']
            if j+offset >= rowcount: continue

            trow = rows[j+offset][i]
            if r['note'] not in [NOTE_OFF,NOTE_NONE,NOTE_FADE,NOTE_CUT]:
                trow['note'] = (c['note'] - cp['note']) + r['note']
            else:
                trow['note'] = r['note']
            if c['inst'] > 0 and r['inst'] > 0:
                trow['inst'] = c['inst']
            else:
                trow['inst'] = r['inst']
            trow['volcmd'] = r['volcmd']
            if c['volval'] > -1 and c['volcmd'] == 'v' and (r['note'] not in [NOTE_OFF,NOTE_NONE,NOTE_FADE,NOTE_CUT] or  r['volcmd'] == 'v'):
                volval = 64
                if r['volcmd'] == 'v' and r['volval'] > -1: volval = r['volval']
                trow['volcmd'] = 'v'
                pvolval = 64
                if (cp['volcmd'] == 'v' and cp['volval'] > -1): pvolval = cp['volval']
                trow['volval'] = int((float(c['volval']) / float(pvolval)) * float(volval))
            elif c['volcmd'] != '' and c['volval'] > -1 and r['note'] not in [NOTE_OFF,NOTE_NONE,NOTE_FADE,NOTE_CUT]:
                trow['volcmd'] = c['volcmd']
                trow['volval'] = c['volval']
            else:
                trow['volval'] = r['volval']
            if c['effcmd'] != '':
                trow['effval'] = c['effval']
                trow['effcmd'] = c['effcmd']
            elif r['effcmd'] not in ['A','B','C']:
                trow['effval'] = r['effval']
                trow['effcmd'] = r['effcmd']
            else:
                trow['effcmd'] = ''
                trow['effval'] = -1
    return mpt_join_pattern(rows)



def SuperCpy3(data):
    rowx = mpt_split_pattern(data)
    xcount = len(map(list, zip(*rowx)))
    for mmm in xrange(0,xcount,2):
        trsp = map(list, zip(*rowx))[mmm:mmm+2]
        rows = map(list, zip(*trsp))
        rowcount = len(rows)
        colcount = len(rows[0])
        colprops = []
        for i,c in enumerate(map(list, zip(*rows))):
            #find first note
            for j,r in enumerate(c):
                if r['note'] not in [NOTE_OFF,NOTE_NONE,NOTE_FADE,NOTE_CUT]:
                    col = {}
                    col['offset'] = j
                    col['note'] = r['note']
                    col['volval'] = r['volval'] if r['volcmd'] in ['v','o','p'] else -1
                    col['volcmd'] = r['volcmd'] if r['volcmd'] in ['v','o','p'] else ''
                    col['effval'] = r['effval'] if r['effcmd'] in ['O','S'] else -1
                    col['effcmd'] = r['effcmd'] if r['effcmd'] in ['O','S'] else ''
                    col['inst'] = r['inst']
                    colprops.append(col)
                    break

        col1 = [x[0] for x in rows]
        cp = colprops[0]
        for j,r in enumerate(col1):
            for i,c in enumerate(colprops):
                if i == 0: continue
                offset = c['offset'] - cp['offset']
                if j+offset >= rowcount: continue

                trow = rows[j+offset][i]
                if r['note'] not in [NOTE_OFF,NOTE_NONE,NOTE_FADE,NOTE_CUT]:
                    trow['note'] = (c['note'] - cp['note']) + r['note']
                else:
                    trow['note'] = r['note']
                if c['inst'] > 0 and r['inst'] > 0:
                    trow['inst'] = c['inst']
                else:
                    trow['inst'] = r['inst']
                trow['volcmd'] = r['volcmd']
                if c['volval'] > -1 and c['volcmd'] == 'v' and (r['note'] not in [NOTE_OFF,NOTE_NONE,NOTE_FADE,NOTE_CUT] or  r['volcmd'] == 'v'):
                    volval = 64
                    if r['volcmd'] == 'v' and r['volval'] > -1: volval = r['volval']
                    trow['volcmd'] = 'v'
                    pvolval = 64
                    if (cp['volcmd'] == 'v' and cp['volval'] > -1): pvolval = cp['volval']
                    trow['volval'] = int((float(c['volval']) / float(pvolval)) * float(volval))
                elif c['volcmd'] != '' and c['volval'] > -1 and r['note'] not in [NOTE_OFF,NOTE_NONE,NOTE_FADE,NOTE_CUT]:
                    trow['volcmd'] = c['volcmd']
                    trow['volval'] = c['volval']
                else:
                    trow['volval'] = r['volval']
                if c['effcmd'] != '':
                    trow['effval'] = c['effval']
                    trow['effcmd'] = c['effcmd']
                elif r['effcmd'] not in ['A','B','C']:
                    trow['effval'] = r['effval']
                    trow['effcmd'] = r['effcmd']
                else:
                    trow['effcmd'] = ''
                    trow['effval'] = -1
    return mpt_join_pattern(rowx)


hotmap = []
hotmap.append({ 
        'title': 'Replace Note Cuts with Note Offs', 
        'short': 'Ctrl+Shift+1',
        'mod': MOD_CONTROL + MOD_SHIFT, 'vk': VK_1, 
        'handler': lambda x: x.replace("^^^","===") })
hotmap.append({ 
        'title': 'Generate Pitch Envelope With Notes', 
        'short': 'Ctrl+Shift+5',
        'mod': MOD_CONTROL + MOD_SHIFT, 'vk': VK_5, 
        'handler': NotesToPitchEnv })
hotmap.append({ 
        'title': 'SuperCPY 2', 
        'short': 'Ctrl+Shift+2',
        'mod': MOD_CONTROL + MOD_SHIFT, 'vk': VK_2, 
        'handler': SuperCpy2 })
hotmap.append({ 
        'title': 'SuperCPY', 
        'short': 'Ctrl+Shift+3',
        'mod': MOD_CONTROL + MOD_SHIFT, 'vk': VK_3, 
        'handler': SuperCpy })
hotmap.append({ 
        'title': 'SuperCPY Parallel', 
        'short': 'Ctrl+Shift+4',
        'mod': MOD_CONTROL + MOD_SHIFT, 'vk': VK_4, 
        'handler': SuperCpy3 })
