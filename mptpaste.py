from script import *

from pysystray import *

from vk import *


import itertools, glob
import functools
import os

def bye(sysTrayIcon): pass
def hello(hk,sysTrayIcon): 
    sysTrayIcon.handlehot(hk)

icons = itertools.cycle(glob.glob('*.ico'))
hover_text = "Modplug Paste Tools"

script = os.path.expanduser("script\\script.py")

execfile(script,globals())

menu_options = ()
for hk in hotmap:
    menu_options += ((hk['title']+'\t'+hk['short'], None, functools.partial(hello,hk)),)

systray = SysTrayIcon(icons.next(), hover_text, menu_options, on_quit=bye, default_menu_index=1, hot_map=hotmap)

systray.go()

